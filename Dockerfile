# Используем базовый образ Ubuntu
FROM python:3.11


# Устанавливаем необходимые пакеты
RUN apt-get update && apt-get install -y \
    python3-pip \
    python3-dev \
    libldap2-dev \
    libsasl2-dev \
    libssl-dev \
    locales \
    lsb-release \
    gcc \
    ffmpeg \
    lame \
    mencoder \
    sox \
    libmp3lame-dev \
    portaudio19-dev \
    libasound2-dev \
    libjack-jackd2-dev

# Обновление локали
RUN echo ru_RU.UTF-8 UTF-8 >> /etc/locale.gen && \
    locale-gen

ENV APPDIR /backend
RUN mkdir -p $APPDIR
WORKDIR $APPDIR

# Копирование requirements.txt внутрь контейнера
COPY requirements.txt $APPDIR/

# Установка зависимостей из requirements.txt
RUN pip3 install --no-cache-dir --upgrade -r requirements.txt

# Копирование остального кода приложения
COPY . $APPDIR/

# Команда для запуска приложения
CMD ["python", "/backend/main.py"]
